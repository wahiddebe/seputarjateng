<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class PenggunaController extends Controller
{
    public function index()
    {
        return view('admin.pengguna.pengguna', [
            'pengguna' => User::latest()->get(),
        ]);
    }


    public function Edit()
    {
        $data = User::where('id', Auth::user()->id)->first();
        return view('admin.pengguna.edit', compact('data'));
    }

    public function UpdateProfile(Request $r)
    {
        $data = User::where('id', Auth::user()->id);


        $upda = [
            'name' => $r->name,
            'email' => $r->email,
        ];

        $data->update($upda);
        return redirect(route('pengguna.edit'))->with('success', 'Data Diri Berhasil Dirubah');
    }

    public function Updatepass(Request $r)
    {
        $user = User::where('id', Auth::user()->id)->first();
        $data = User::where('id', Auth::user()->id);

        if (Hash::check($r->password_old, $user->password)) {
            $upda = [
                'password' => bcrypt($r->password_new)
            ];
        } else {
            return redirect(route('pengguna.edit'))->with('danger', 'Kata Sandi Lama Salah, Silahkan coba kembali');
        }

        $data->update($upda);
        return redirect(route('pengguna.edit'))->with('success', 'Password Berhasil Dirubah');
    }
}
