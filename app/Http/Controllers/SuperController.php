<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Artikel;
use App\Contact;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class SuperController extends Controller
{
    public function Index()
    {

        return view('super.login');
    }

    public function Login(Request $r)
    {

        $username = $r->username;
        $password = $r->password;

        $data = Admin::where('username', $username)->first();
        if ($data) { //apakah username tersebut ada atau tidak
            if (Hash::check($password, $data->password)) {
                Session::put('username', $data->username);
                Session::put('id', $data->id);

                Session::put('name', $data->name);
                Session::put('logins', TRUE);
                Session::put('logins', TRUE);
                return redirect(route('home.admin'));
            } else {
                return redirect(route('login.super'))->with('alert', 'Password atau Username, Salah !');
            }
        } else {
            return redirect(route('login.super'))->with('alert', 'Password atau Username, Salah!');
        }
    }

    public function home()
    {
        $j_artikel = Artikel::get()->count();
        $j_pesan = Contact::get()->count();

        return view('super.dashboard', compact('j_artikel', 'j_pesan'));
    }

    public function pengguna()
    {
        $data = User::get();
        return view('super.pengguna', compact('data'));
    }
    public function addp()
    {
        return view('super.addp');
    }

    public function add(Request $r)
    {
        User::create([
            'name' => $r->name,
            'email' => $r->email,
            'password' => hash::make('12345678')
        ]);

        return redirect(route('pengguna.admin'));
    }
    public function deletep($id)
    {
        $data = User::findOrFail($id);
        $data->delete();
        return redirect(route('pengguna.admin'));
    }

    public function reset($id)
    {

        $data = User::where('id', $id);
        $upda = [
            'password' => hash::make('12345678')
        ];

        $data->update($upda);
        return redirect(route('pengguna.admin'));
    }

    public function setprofile()
    {
        $data = Admin::where('id', Session::get('id'))->first();

        return view('super.setting', compact('data'));
    }
    public function updateprofile(Request $r)
    {
        $user = Admin::where('id', Session::get('id'))->first();
        $data = Admin::where('id', Session::get('id'));

        $upda = [
            'username' => $r->username,

        ];
        $data->update($upda);
        return redirect(route('set.profile'))->with('success', 'Data Diri Berhasil Dirubah');
    }

    public function updatepass(Request $r)
    {
        $user = Admin::where('id', Session::get('id'))->first();
        $data = Admin::where('id', Session::get('id'));



        if (Hash::check($r->password_old, $user->password)) {
            $upda = [
                'password' => bcrypt($r->password_new)
            ];
        } else {
            return redirect(route('set.profile'))->with('danger', 'Kata Sandi Lama Salah, Silahkan coba kembali');
        }

        $data->update($upda);
        return redirect(route('set.profile'))->with('success', 'Password Anda Berhasil Dirubah');
    }
}
