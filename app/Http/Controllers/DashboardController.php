<?php

namespace App\Http\Controllers;

use App\Artikel;
use App\Contact;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $j_artikel = Artikel::get()->count();
        $j_pesan = Contact::get()->count();

        return view('admin.dashboard', compact('j_artikel', 'j_pesan'));
    }
}
