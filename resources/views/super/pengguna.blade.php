@extends('layouts.super.master')
@section('content')


<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Media Sosial</h1>
        <a class="btn btn-success" href="{{route('add.pengguna')}}">Tambah Pengguna</a>
    </div>

    <div class="row">


        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4 p-2">
                <table width="100%" class=" table table-striped table-bordered table-hover" id="">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>
                                <div class="button-group">
                                    <a href="{{route('reset.pengguna',  $item->id  )}}"><button class="btn-warning btn">Reset Password</button></a>
                                    <a href="{{route('delete.pengguna',  $item->id  )}}"><button class="btn-danger btn">Delete</button></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>
@endsection