@extends('layouts.super.master')
@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Media Sosial</h1>
    </div>



    <!-- Content Row -->

    <div class="row">


        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <form action="{{route('tambah.pengguna')}}" method="post" class="m-3" enctype="multipart/form-data" class="m-3">
                    @csrf

                    <div class="form-group">
                        <label>Name</label>
                        <input name="name" value="" class="form-control" type="text" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input name="email" value="" class="form-control" type="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>


</div>

@endsection