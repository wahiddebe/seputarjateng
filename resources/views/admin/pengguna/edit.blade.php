@extends('layouts.admin.master')
@section('content')

<div class="container-fluid">
    @if(session()->has('danger'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Peringatan !</h4>
        {!! session()->get('danger') !!}
    </div>
    @elseif(session()->has('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Peringatan !</h4>
        {!! session()->get('success') !!}
    </div>
    @endif

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Profile</h1>
    </div>



    <!-- Content Row -->

    <div class="row">


        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <form action="{{route('pengguna.update.profile')}}" method="post" class="m-3" enctype="multipart/form-data" class="m-3">
                    @csrf
                    @method('patch')
                    <h3>Data Diri</h3>
                    <div class="form-group">
                        <label>Nama</label>
                        <input name="name" value="{{$data->name}}" class="form-control" type="text" placeholder="Nama">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input name="email" value="{{$data->email}}" class="form-control" type="Email" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </form>
                <hr>
                <form action="{{route('pengguna.update.pass')}}" method="post" class="m-3" enctype="multipart/form-data" class="m-3">
                    @csrf
                    @method('patch')
                    <h3>Password</h3>
                    <div class="form-group">
                        <label>Password Lama</label>
                        <input name="password_old" value="" class="form-control" type="password" placeholder="Password Lama">
                    </div>
                    <div class="form-group">
                        <label>Password Baru</label>
                        <input name="password_new" value="" class="form-control" type="password" placeholder="Password Baru">
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>


</div>


@endsection